# Go - Checksum Helper

Convenient functions for generating checksums from common datatypes.

## Installation

```shell
go get -u gitlab.com/tkrause/go-checksum
```

## Usage

Import the package

```go
import "gitlab.com/tkrause/go-checksum"
```

Be aware, that since _go-checksum_ uses the crypto library for hash computation. There is a possibility for **panic**. Be sure to catch it with **recover()** if hash is not available on all platforms.

Use and abuse til your hearts content!

## Examples

Checksum from File

```go
str, err := checksum.File("path/to/file", crypto.MD5)
if err != nil {
        // Handle errors here
}

// Do stuff with checksum stored in str
```

Checksum from String

```go
str := checksum.String("This is some text to checksum", crypto.SHA1)
// Do stuff with checksum stored in str. 
```

Checksum from URL

```go
str, err := checksum.Url("http://google.com", crypto.SHA512)
if err != nil {
        // Handle errors here
}

// Do stuff with str
```

