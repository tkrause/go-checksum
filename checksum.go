package checksum

import (
	"os"
	"crypto"
	"io"
	"encoding/hex"
	"net/http"
	// Support the following algorithms out of the box
	_ "crypto/sha1"
	_ "crypto/sha256"
	_ "crypto/sha512"
	_ "crypto/md5"
)

/**
Read Input from file and hash the contents with the desired algorithm
 */
func File(filePath string, algorithm crypto.Hash) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}

	defer file.Close()
	hash := crypto.Hash(algorithm).New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}
/**
Hash the contents of a string
 */
func String(str string, algorithm crypto.Hash) string {
	hash := crypto.Hash(algorithm).New()
	hash.Write([]byte(str))
	return hex.EncodeToString(hash.Sum(nil))
}
/**
Perform a GET HTTP request to the url provided and hash it's response Body
 */
func Url(url string, algorithm crypto.Hash) (string, error) {
	response, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer response.Body.Close()
	hash := crypto.Hash(algorithm).New()
	if _, err := io.Copy(hash, response.Body); err != nil {
		return "", err
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}