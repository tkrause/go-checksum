package checksum

import (
	"testing"
	"crypto"
	"strings"
)

const (
	TargetUrl = "https://gitlab.com/tkrause/go-checksum/raw/master/test.txt"
	UrlResult = "f95ff3a243cc80a5c7c7263a5dd141921beb2edc"
	FileResult = "f95ff3a243cc80a5c7c7263a5dd141921beb2edc"
	StringResult = "4c0d2b951ffabd6f9a10489dc40fc356ec1d26d5"
)

func TestFile(t *testing.T) {
	str, err := File("test.txt", crypto.SHA1)
	if strings.ToLower(str) != FileResult || err != nil {
		if err != nil {
			t.Fatal(err)
		} else {
			t.Fatalf("(SHA1-File) '%s' does not match '%s'", str, FileResult)
		}
	}
}

func TestString(t *testing.T) {
	str := String("testing123", crypto.SHA1)
	if strings.ToLower(str) != StringResult {
		t.Fatalf("(SHA1-String) '%s' does not match '%s'", str, StringResult)
	}
}

func TestUrl(t *testing.T) {
	str, err := Url(TargetUrl, crypto.SHA1)
	if strings.ToLower(str) != UrlResult || err != nil {
		if err != nil {
			t.Fatal(err)
		} else {
			t.Fatalf("(SHA1-Url) '%' does not match '%'", str, UrlResult)
		}
	}
}